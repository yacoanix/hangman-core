const hideWord = require('../lib/hide-word');

test('Hides a word with spaces', () => {
  expect(hideWord('this is a test')).toEqual('____ __ _ ____')
});

test('Hides a word with punctuation', () => {
  expect(hideWord('testing!!')).toEqual('_______!!')
})
