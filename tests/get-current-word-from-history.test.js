const getCurrentWordFromHistory = require('../lib/get-current-word-from-history');

test('Get the current word from a history of guess', () => {
  expect(getCurrentWordFromHistory('testing', ['a', 'e', 't', 'n', 'f'])).toEqual('te_t_n_')
})
