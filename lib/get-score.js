const START_SCORE = 100;
const SCORE_STEP = 10;

module.exports = function getScore(word, history) {
  return history.reduce((prev, letter) => (
    !word.includes(letter)
      ? prev - SCORE_STEP
      : prev
  ), START_SCORE)
}
