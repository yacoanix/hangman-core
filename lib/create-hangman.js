const pickFromArray = require('./pick-random-from-array');
const getStateFromHistory = require('./get-state-from-history');

module.exports = function createHangman(words) {
  let history, word;

  const initialize = () => {
    word = pickFromArray(words);
    history = [];
  }

  const play = letter => {
    if (letter) { history.push(letter); }
    return getStateFromHistory(word, history);
  }

  initialize();

  return { play, reset: initialize };
}
