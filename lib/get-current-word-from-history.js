const hideWord = require('./hide-word');
const getPositions = require('./get-positions');
const replacePositions = require('./replace-positions');

module.exports = function getCurrentWordFromHistory(word, history) {
  return history.reduce((prev, letter) => (
    replacePositions(prev, getPositions(word, letter), letter)
  ), hideWord(word))
}
